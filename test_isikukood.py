import isikukood
import unittest

class TestIsikukood(unittest.TestCase):
    def test_checksum(self):
        self.assertEqual("2", isikukood.checksum("1881118000"))

    def test_find_bday(self):
        self.assertEqual('18.11.1888', isikukood.find_bday("1881118000"))
