from random import randint
from datetime import date


def ee_person_code():
    """ Returns Estonian person code. """
    code = datepart() + randompart()
    code += checksum(code)
    return code


def datepart():
    """ Returns a random first part for the EE person code. """
    year = randint(1800, date.today().year - 1)
    month = randint(1, 12)
    day = randint(1, 28)
    birthdate = date(year, month, day)
    first = find_first(birthdate.year)
    str_date = birthdate.strftime("%Y%m%d")
    return first + str_date[2:]


def find_first(year):
    """ Returns the first number of the person code. """
    if year < 1900:
        return str(randint(1, 2))
    elif year < 2000:
        return str(randint(3, 4))
    elif year < 2100:
        return str(randint(5, 6))


def randompart():
    num = str(randint(0, 999))
    while len(num) < 3:
        num = "0" + num
    return num


def checksum(code):
    """ Returns the checksum for the code. """
    #result = 0
    for weights in ((1, 2, 3, 4, 5, 6, 7, 8, 9, 1), (3, 4, 5, 6, 7, 8, 9, 1, 2, 3)):
        result = sum(weight * int(num) for weight, num in zip(weights, code)) % 11
        if result < 10:
            return str(result)
    return str(result)

def find_bday(code):
    year_begins = {"1": 1800, "2": 1800, "3": 1900, "4": 1900, "5": 2000, "6": 2000}
    year = str(year_begins[code[0]] + int(code[1:3]))
    month = code[3:5]
    day = code[5:7]
    return "%s.%s.%s" % (day, month, year)

if __name__ == "__main__":
    print(ee_person_code())