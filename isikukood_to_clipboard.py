
from tkinter import Tk
import isikukood

def to_clipboard(text):
    root = Tk()
    root.withdraw()
    root.clipboard_clear()
    root.clipboard_append(text)
    root.mainloop()
    #root.destroy()

def isikukood_to_clipboard():
    to_clipboard(isikukood.ee_person_code())

if __name__ == "__main__":
    isikukood_to_clipboard()